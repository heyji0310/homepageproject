<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>혜지의 공간</title>
<link rel="stylesheet" href="<c:url value='/jsp/homepage/css/mainpage.css?after'/>" >
<link rel="stylesheet" href="<c:url value='/jsp/homepage/css/login.css?after'/>">

<script src="https://kit.fontawesome.com/a1cdd9b3c8.js" crossorigin="anonymous"></script>

<style>
body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>") no-repeat;
	background-size: 100%; 
}
</style>

</head>
<body>


	<div id="container">
		<header id="header" style="text-align: center;">
			<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp" %>
		</header>
		<aside id="sidebar">
			<%@ include file="/jsp/homepage/includes/commonPagesSubSide.jsp" %>
		</aside>
		
		<section id="contents">
			
		
			<div class="button-width" align="right" style="margin: 30px">
			<button class="w-btn-outline w-btn-indigo-outline"
				id="login" value="로그아웃" onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'> 로그아웃</button>
			</div>
				
				
			<img width="1100px" height="auto" src="<c:url value='/jsp/homepage/videos/wellcomeVideo.gif'/>"></img>
			
				
		</section>
	</div>
<footer>
	<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp" %>
</footer>

</body>
</html>