<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<script type="text/javascript" src="/jsp/storyBoard/postDetail.js"></script>

<title>게시글 정보</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

<script type="text/javascript" src="/jsp/storyBoard/postDetail.js"></script>


</head>
<body>
<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesSubSide.jsp"%>
		
			<section id="contents">
		
			<div class="button-width" align="right" style="margin: 30px">
			<button class="w-btn-outline w-btn-indigo-outline"
				id="login" value="로그아웃" onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'> 로그아웃</button>
			</div>

			<h1>게시글 상세보기</h1>
			<br/>
			<table id="memberInfo">
				<tr>
					<th>No.</th>
					<th>제목</th>
					<th>작성자</th>
					<th>작성일자</th>
					<th>수정일자</th>
					<th>조회수</th>
					<th>좋아요</th>
				</tr>
				
				<tr>
					<td>${boardBean.boardNo}</td>
					<td>${boardBean.postTitle}</td>
					<td>${boardBean.postWriter}</td>
					<td>${boardBean.dateCreate}</td>
					<td>${boardBean.dateUpdate}</td>
					<td>${boardBean.viewsCount}</td>
					<td>${boardBean.likesCount}</td>
				</tr>
				<tr>
				<th rowspan=2>내용</th>
				</tr>
				<tr>
				<td colspan=6>${boardBean.postContent}</td>
				</tr>
			</table>


			<button style="margin-left: 20px;"
				onclick="jsPostUpdate()">글 수정하기</button>
				
			<button style="margin-left: 20px;"
				onclick="jsPostDelete()">삭제하기</button>
				
			<a href="<c:url value='/storyBoard/postList.do'/>">목록 보기</a>
</section>

<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
		
			</div>
		

<script type="text/javascript">
function jsPostUpdate() {
	location.href='<c:url value="/storyBoard/updatePostForm?boardNo=${boardBean.boardNo}"/>';
}

function jsPostDelete() {
	location.href='<c:url value="/storyBoard/deletePost?boardNo=${boardBean.boardNo}"/>';
}
</script>
</body>
</html>