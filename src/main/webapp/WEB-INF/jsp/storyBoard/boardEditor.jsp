<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>매너있는 글 작성 부탁합니다</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>


<script src="https://cdn.ckeditor.com/ckeditor5/35.3.2/classic/ckeditor.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/35.3.2/classic/translations/ko.js"></script>


</head>
<body>


	<div id="container">
	<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesSubSide.jsp"%>
		
		<section id="contents">
		
		<div class="button-width" align="right" style="margin: 30px">
			<button class="w-btn-outline w-btn-indigo-outline"
				id="login" value="로그아웃" onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'> 로그아웃</button>
			</div>
		

		
		<form name="postForm" id="postForm" method="post" enctype="multipart/form-data">
				
	<label>제목</label>
	<input type="text" name="postTitle" id="postTitle" /><br/>
	<br/>
	<label>작성자</label>
	<input type="text" name="postWriter" id="postWriter" value="${sess_memberBean.memberId}" readonly="readonly" /><br/>
	<br/>
	<label>첨부파일</label>
	<input type="file" name="filename1" id="filename1" /><br/>
	<br/>
	<label>내용</label>
	<textarea name="postContent" id="postContent"></textarea>
	
	
	<br/>
	<input id="sendPosting" name="sendPosting" type="submit" value="글 등록">
	
	<a href="<c:url value='/storyboard/postList'/>" style="margin-left: 30px">뒤로가기</a>
	
	
</form>
 <script>
        ClassicEditor
            .create( document.querySelector( '#postContent' ), {language : "ko"} )
            .catch( error => {
                console.error( error );
        } );
</script>

<script type="text/javascript">
	$(#"title").val("제목출력함");

/*
let boardForm = document.querySelector("#postForm");
boardForm.addEventListener("submit", (e) => {
	e.preventDefault();
	
	fetch('/homepageProject/storyboard/insert', {		
		method : 'POST',
	    cache: 'no-cache',
		body: new FormData(boardForm)		
	})
	.then(response => response.json())
	.then(jsonResult => {
		alert(jsonResult.message);
		if (jsonResult.status == true) {
			location.href = jsonResult.url;
		}
	});
});
*/
</script>
		
		</section>
		
		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
		
	</div>

</body>
</html>