window.onload = () => {
	const updatePost = document.querySelector("#updatePost");
	const postTitle = document.querySelector("#postTitle");
	const postContent = document.querySelector(".ck-content");
	alert("매너있는 글 작성 부탁합니다.");
	updatePost.addEventListener("click", async e => {
		alert("매너있는 글 작성 감사합니다.")
		e.preventDefault();
		const reqJSON = {
			  boardNo : document.querySelector("#boardNo").value
			, postTitle : postTitle.value
			, postContent : postContent.innerHTML
		};
		
		const response = await fetch(`/homepageProject/storyboard/updatePost?${new URLSearchParams(reqJSON).toString()}`);
		
		const json = await response.json();
		console.log(json);
		alert(json.message);
		if (json.status) {
			location.href = json.url;
		}
	})
}