<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>매너있는 글 수정 부탁합니다</title>
<script type="text/javascript"
	src="<c:url value='/jsp/storyboard/updatePost.js'/>"></script>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true" />

<script
	src="https://cdn.ckeditor.com/ckeditor5/35.3.2/classic/ckeditor.js"></script>
<script
	src="https://cdn.ckeditor.com/ckeditor5/35.3.2/classic/translations/ko.js"></script>


</head>
<body>


	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>

		<%@ include file="/jsp/homepage/includes/commonPagesSubSide.jsp"%>

		<section id="contents">

			<form id="updatePostForm">
				<label>No.</label> <input type="text" name="boardNo" id="boardNo"
					value="${boardBean.boardNo}" readonly="readonly"><br /> <label>제목</label>
				<input type="text" name="postTitle" id="postTitle"
					value="${boardBean.postTitle}"><br /> <label>작성자</label> <input
					type="text" name="postWriter" id="postWriter"
					value="${boardBean.postWriter}" readonly="readonly"><br />
				<label>내용</label>
				<textarea name="postContent" id="postContent">${boardBean.postContent}</textarea>

				<input id="updatePost" type="submit" value="글 수정"> <a
					href="<c:url value='/storyboard/postList'/>">뒤로가기</a>

			</form>
			<script>
        ClassicEditor
            .create( document.querySelector( '#postContent' ), {language : "ko"} )
            .catch( error => {
                console.error( error );
        } );
</script>

		</section>

		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>

	</div>

</body>
</html>