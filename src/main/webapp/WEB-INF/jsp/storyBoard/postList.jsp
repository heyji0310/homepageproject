<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>		
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>공지사항을 확인하세요</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

<script type="text/javascript" src="./postList.js"></script>
  
</head>

<body>

	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesSubSide.jsp"%>
		
		<section id="contents">
		
			<div class="button-width" align="right" style="margin: 30px">
			<button class="w-btn-outline w-btn-indigo-outline"
				id="login" value="로그아웃" onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'> 로그아웃</button>
			</div>
				
<h1>게시글 목록</h1>
<br/>

<table id="boardsInfomations">
		<tr>
			<th>No.</th>
			<th>제목</th>
			<th>작성자</th>
			<th>최종작성일자</th>
			<th>조회수</th>
			<th>좋아요</th>
		</tr>
		
		<c:forEach var="board" items="${listBoards}">
			<tr>
				<td>${board.boardNo}</td>
				<td><a href="/homepageProject/storyBoard/postDetail.do?boardNo=${board.boardNo}">${board.postTitle}</a></td>
				<td>${board.postWriter}</td>
				<td>${board.dateUpdate}</td>
				<td>${board.viewsCount}</td>
				<td>${board.likesCount}</td>
			</tr>
		</c:forEach>
		
		
		
</table>


<div id="left">
			<button id="firstPage">처음</button>
			<button id="prevSet">&lt;</button>
			<div id="pageButtons" style='display: inline-block'>
				<button data-page='1'>1</button>
				<button data-page='2'>2</button>
				<button data-page='3'>3</button>
				<button data-page='4'>4</button>
				<button data-page='5'>5</button>
				<button data-page='6'>6</button>
				<button data-page='7'>7</button>
				<button data-page='8'>8</button>
				<button data-page='9'>9</button>
				<button data-page='10'>10</button>
			</div>
			<button id="nextSet">&gt;</button>
			<button id="lastPage">마지막</button>
		</div>

			<button style="margin-left: 20px;"
				onclick="location.href='/homepageProject/jsp/storyboard/boardEditor.jsp'">글쓰기</button>
			
			
		</section>

<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
		
		<form name="detailForm" action="<c:url value='/storyboard/postDetail'/>" method="post">
			<input type="hidden" name="boardNo" id="boardNo">
		</form>
		
		
	</div>

</body>

	<script>
		function postDetail(boardNo) {
			var form = detailForm;
			form.boardNo.value = boardNo;
			form.submit();
		}
	</script>

</html>