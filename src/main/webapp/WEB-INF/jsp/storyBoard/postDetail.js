window.onload = () => {
	const postDetail = document.querySelector("#postDetail");
	const boardNo = document.querySelector("#boardNo");
	const boardTitle = document.querySelector("#boardTitle");
	const boardWriter = document.querySelector("#boardWriter");
	const dateCreate = document.querySelector("#dateCreate");
	const dateUpdate = document.querySelector("#dateUpdate");
	const viewsConut = document.querySelector("#viewsConut");
	const likesConut = document.querySelector("#likesConut");
	const boardContent = document.querySelector("#boardContent");
	
	
	postDetail.addEventListener("click", async e => {
		e.preventDefault();
		const reqJSON = {
			boardNo : boardNo.value
			, boardTitle : boardTitle.value
			, boardWriter : boardWriter.value
			, dateCreate : dateCreate.value
			, dateUpdate : dateUpdate.value
			, viewsConut : viewsConut.value
			, likesConut : likesConut.value
			, boardContent : boardContent.value
			
		};
		
		const response = await fetch(`/homepageProject/storyboard/postDetail?${new URLSearchParams(reqJSON).toString()}`);
		
		const json = await response.json();
		console.log(json);
		alert(json.message);
		if (json.status) {
			location.href = json.url;
		}
	})
}
