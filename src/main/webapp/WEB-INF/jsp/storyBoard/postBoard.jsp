<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>공지사항을 확인하세요</title>
<link rel="stylesheet"
	href="<c:url value='/jsp/homepage/css/mainpage.css?after'/>">
<script src="https://kit.fontawesome.com/a1cdd9b3c8.js" crossorigin="anonymous"></script>

<style>
table, th, td {
	border: 1px dotted #ccc;
	border-collapse: collapse;
}

th, td {
	padding: 10px 20px;
}

body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>")
		no-repeat;
	background-size: 100%;
}
</style>
</head>
<body>

	<div id="container">
		<header id="header" style="text-align: center;">
			<a href="<c:url value='/homepageMain.jsp'/>">
			<img alt="logo" src="<c:url value='/jsp/homepage/images/logo.png'/>"
				width="15%">
		  </a>
		</header>


		<aside id="sidebar">
			<h1>MENU</h1>

			<nav>
				<ul id="homeMenu">
					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="HOME"
						href="<c:url value='/homepageSub.jsp'/>"><i class="fa-solid fa-house-chimney" style="color:#0056ff"></i>HOME</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="MY_PAGE"
						href="<c:url value='/jsp/member/view.jsp'/>">MY_PAGE</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="CHATTING"
						href="<c:url value='/jsp/chatting/chattingRoom.jsp'/>">CHATTING</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now">
					<a class="menu-name" alt="STORYBOARD" 
						href="<c:url value='/storyboard/postList'/>">STORYBOARD</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>
				</ul>
			</nav>
		</aside>


		<section id="contents">

			<button id="login" value="로그아웃"
				onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'>
				로그아웃</button>

			<h1>회원 정보</h1>
			<table id="memberInfo">
				<tr>
					<th>No.</th>
					<th>제목</th>
					<th>내용</th>
					<th>작성자</th>
					<th>작성일자</th>
					<th>조회수</th>
					<th>좋아요</th>
				</tr>

				<tr>
					<td>${storyBoard.boardNo}</td>
					<td>${storyBoard.postTitle}</td>
					<td>${storyBoard.postContent}</td>
					<td>${storyBoard.dateCreate}</td>
					<td>${storyBoard.dateUpdate}</td>
					<td>${storyBoard.viewsCount}</td>
					<td>${storyBoard.likesCount}</td>
				</tr>
			</table>


			<button style="margin-left: 20px;"
				onclick="location.href='/homepageProject/jsp/member/updateMemberInfo.jsp'">수정하기</button>
			<button style="margin-left: 20px;"
				onclick="location.href='/homepageProject/jsp/member/withdrawalMember.jsp'">탈퇴하기</button>
		</section>



		<footer id="footer">
			<h4>제작자:안혜지</h4>
		</footer>

	</div>
</body>
</html>