<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>탈퇴하시나요</title>
<script type="text/javascript" src="./withdrawalMember.js"></script>
<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>

<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>

</head>

<body>

	<div id="container">
		<section id="contents">
			<div>
				<h1>
					<span id="title">회원 탈퇴</span>
				</h1>
				<br />
			</div>
			<!-- form name="joinMemberForm" action="/homepageProject/member/insert" method="post" enctype="utf-8"-->
			<form id="withdrawalForm" name="withdrawalForm"
				action="<c:url value='/Member/withdrawalMember'/>">

				<label for="memberId">아이디</label> <input type="text" id="memberId"
					name="memberId" value="${sess_memberBean.memberId}"
					readonly="readonly" />님 탈퇴하시겠습니까? <br /> <br /> <input
					id="withdrawal" name="withdrawal" type="submit" value="탈퇴하기">
				<a href="/homepageProject/jsp/member/view.jsp"
					style="margin-left: 10px">돌아가기</a>



			</form>
		</section>

		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>