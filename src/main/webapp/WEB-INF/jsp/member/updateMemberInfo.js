window.onload = () => {
	const updateMemberInfo = document.querySelector("#updateMemberInfo");
	const memberPwd = document.querySelector("#memberPwd");
	const memberName = document.querySelector("#memberName");
	const memberEmail = document.querySelector("#memberEmail");
	const phone = document.querySelector("#phone");
	const address = document.querySelector("#address");
	const birthday = document.querySelector("#birthday");
	
/*	
	const registerForm = document.querySelector("#registerForm");
	const req = {}
	new FormData(registerForm).forEach((value, key) => req[key] = value);
	console.log(req);
*/	
	updateMemberInfo.addEventListener("click", async e => {
		e.preventDefault();
		const reqJSON = {
			memberPwd : memberPwd.value
			, memberName : memberName.value
			, memberEmail : memberEmail.value
			, phone : phone.value
			, address : address.value
			, birthday : birthday.value
			, gender : document.querySelector("input[name=gender]:checked").value
		};
		
		const response = await fetch(`/homepageProject/member/updateMemberInfo?${new URLSearchParams(reqJSON).toString()}`);
		
		const json = await response.json();
		console.log(json);
		alert(json.message);
		if (json.status) {
			location.href = json.url;
		}
	})
}