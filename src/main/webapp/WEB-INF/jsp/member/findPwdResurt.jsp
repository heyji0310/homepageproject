<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>비밀번호를 찾아주세요</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

</head>
<body>



	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>
		
		<section id="contents">
		
	${message}

<form action="<c:url value='/jsp/member/findPwdForm'/>" name="pwdLoginAgain" method="post" enctype="utf-8">
	<div class="btn-wrapper">
		<a href="<c:url value='/member/findPwdForm.jsp'/>">뒤로가기</a>
		<a href="<c:url value='/member/loginForm.jsp'/>">로그인 하러가기</a>
	</div>
</form>
</section>
	
	<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>