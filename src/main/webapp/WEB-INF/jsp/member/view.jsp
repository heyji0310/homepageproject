<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원 정보</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

</head>
<body>

	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesSubSide.jsp"%>
		
		<section id="contents">

			<div class="button-width" align="right" style="margin: 30px">
			<button class="w-btn-outline w-btn-indigo-outline"
				id="login" value="로그아웃" onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'> 로그아웃</button>
			</div>

			<h1>회원 정보</h1>
			<br/>
			<table id="memberInfo">
				<tr>
					<th>아이디</th>
					<th>비밀번호</th>
					<th>이름</th>
					<th>이메일</th>
					<th>연락처</th>
					<th>주소</th>
					<th>생일</th>
					<th>성별</th>
					<th>가입일자</th>
				</tr>

				<tr>
					<td>${sess_memberBean.memberId}</td>
					<td>${sess_memberBean.memberPwd}</td>
					<td>${sess_memberBean.memberName}</td>
					<td>${sess_memberBean.memberEmail}</td>
					<td>${sess_memberBean.phone}</td>
					<td>${sess_memberBean.address}</td>
					<td>${sess_memberBean.birthday}</td>
					<td>${sess_memberBean.gender}</td>
					<td>${sess_memberBean.joinDate}</td>
				</tr>
			</table>
			
			
			<button style="margin-left: 20px;"
					onclick="location.href='/member/updateMemberInfo.do'">수정하기</button>
			<button style="margin-left: 20px;"
					onclick="location.href='/member/withdrawalMember.do'">탈퇴하기</button>
		</section>
				
		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
		
	</div>
</body>
</html>