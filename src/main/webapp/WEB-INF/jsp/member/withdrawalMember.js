window.onload = () => {
	
	console.log("탈퇴할수있을까");
	const withdrawal = document.querySelector("#withdrawal");
	const memberId = document.querySelector("#memberId");
	console.log(memberId);
	
	withdrawal.addEventListener("click", async e => {
		e.preventDefault();
		const reqJSON = {
			memberId: memberId.value
		};
		
		const response = await fetch(`/homepageProject/member/withdrawalMember?${new URLSearchParams(reqJSON).toString()}`);
		
		const json = await response.json();
		console.log(json);
		alert(json.message);
		if (json.status) {
			location.href = json.url;
		}
	})
}