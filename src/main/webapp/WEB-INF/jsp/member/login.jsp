<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>로그인 해주세요</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

</head>
<body>

	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>

		<section id="contents">

			<div id="title" class="title">
				<h1>
					<span>로그인</span>
				</h1>
			</div>
			
  <div id="loginLayoutCenter">
  
			<form action="<c:url value='/member/login'/>" name="loginForm"
				id="loginForm" method="post" enctype="utf-8">
			<div class="mainLogin">
				<div>
					<label for="loginId">아이디</label> 
					<input id="loginId" name="loginId"
						type="text" class="login box-size" autofocus="autofocus" style="margin:20px" />
				</div>

				<div>
					<span class="login">비밀번호</span> 
					<input id="loginPwd"
						name="loginPwd" type="password" class="login box-size" style="margin:20px"/>
				</div>

				<div id="loginSignupErrorMsg" class="error-msg"></div>

				<div id="login" class="error-msg"></div>

		<div id="box-outline" align="center" style="width:500px" style="height:180px">
		<div id="box-inline" align="right" style="margin: 30px">
				
				<!-- <div class="button-width" align="center" style="margin: 30px"> -->
					<a class="w-btn-outline w-btn-indigo-outline" 
						href="#" id="loginButton">로그인하기</a>

					<a class="w-btn-outline w-btn-indigo-outline" style="margin: 30px;" style="float:left"
					id="register" href="<c:url value='/member/joinMemberForm.do'/>">회원가입</a>
				</div>


				<div id="findId">
					<a id="findId" href="<c:url value='/member/findIdForm.do'/>">아이디찾기</a>
					<a id="findPwd" href="<c:url value='/member/findPwdForm.do'/>"
						style="margin-left: 20px;">비밀번호 찾기</a>
				</div>
				</div>
				
				</div>
			</form>
  </div>
			<script type="text/javascript">
			loginButton.addEventListener("click", async e => {
				if (loginId.value == "") {
					alert("아이디를 입력해주세요");
					loginId.focus();
					return false;
				}
				if (loginPwd.value == "") {
					alert("비밀번호를 입력해주세요");
					loginPwd.focus();
					return false;
				}
				const response = await fetch("<c:url value='/member/login.do'/>", {
					method: 'POST'
					, headers: {
						'Content-Type': `application/json;charset=utf-8`
					}
					, body: JSON.stringify({
						loginId: loginId.value
						, loginPwd: loginPwd.value
					})
				});
				const json = await response.json();
				alert(json.message);
				if (json.status) {
					location.href = json.url;							
				}
			});
			</script>


		</section>
		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>





</body>
</html>