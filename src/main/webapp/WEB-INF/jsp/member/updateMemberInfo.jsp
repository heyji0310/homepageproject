<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원정보 수정페이지</title>
<script type="text/javascript" src="./updateMemberInfo.js"></script>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true" />

</head>

<body>

	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>

		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>

			<div class="button-width" align="right" style="margin: 30px">
			<button class="w-btn-outline w-btn-indigo-outline"
				id="login" value="로그아웃" onclick='location.href="<c:url value='/jsp/member/logOut.jsp'/>"'> 로그아웃</button>
			</div>

		<section id="contents">

			<div>
				<h1>
					<span id="title">회원 정보 수정</span>
				</h1>
			</div>
			<!-- form name="joinMemberForm" action="/homepageProject/member/insert" method="post" enctype="utf-8"-->
			<form id="memberInfoUpdateForm">
				<div align="center" style="width: 350px" style="height:180px">
					<div id="box-inline" align="right" style="margin: 30px">
						<label for="memberId">아이디</label> <input type="text" id="memberId"
							name="memberId" value="${sess_memberBean.memberId}"
							readonly="readonly" /> <br /> <label for="memberPwd">비밀번호
							*</label> <input type="password" id="memberPwd" name="memberPwd"
							autocomplete="off" autofocus="autofocus"
							placeholder="6자 이상 입력해주세요." /> <br /> <label for="memberName">이름
							*</label> <input type="text" id="memberName" name="memberName"
							autocomplete="off" /> <br /> <label for="memberEmail">이메일
							*</label> <input type="text" id="memberEmail" name="memberEmail"
							autocomplete="off" /> <br /> <label for="phone">휴대폰 번호 *</label>
						<input type="tel" id="phone" name="phone" autocomplete="off"
							maxlength="11" placeholder="숫자만 입력해주세요." /> <br /> <label
							for="address">주소 *</label> <input type="text" id="address"
							name="address" autocomplete="off" /> <br /> <label for="birthday">생년월일
							*</label> <input type="date" id="birthday" name="birthday"
							autocomplete="off" /> <br /> <label for="gender">성별 *</label> <input
							type="radio" id="gender_M" name="gender" value="M"
							autocomplete="off" checked /> <label for="gender_M">남성</label> <input
							type="radio" id="gender_F" name="gender" value="F"
							autocomplete="off" /> <label for="gender_F">여성</label>
					</div>
				</div>

				<input id="updateMemberInfo" type="submit"
					style="margin-left: 100px" value="수정하기"> <input
					type="reset" style="margin-left: 10px" value="다시입력"> <a
					href="/homepageProject/jsp/member/view.jsp"
					style="margin-left: 10px">돌아가기</a>

			</form>

		</section>
		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>