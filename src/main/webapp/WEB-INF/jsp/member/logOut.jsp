<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>로그아웃</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>
</head>
<body>

<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>
	
		<section id="contents">

	<h1>안녕히가세요!</h1>
	
	</section>
		
	<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>