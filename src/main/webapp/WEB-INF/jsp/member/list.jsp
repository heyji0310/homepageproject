<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원 목록</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true" />

</head>
<body>

	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>

		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>

		<section id="contents">

			<h1>회원 목록</h1>
			<table id="membersInfomations">
				<tr>
					<th>아이디</th>
					<th>비밀번호</th>
					<th>이름</th>
					<th>이메일</th>
					<th>연락처</th>
					<th>주소</th>
					<th>생일</th>
					<th>성별</th>
					<th>가입일자</th>
				</tr>

				<c:forEach var="member" items="${listMembers}">
					<tr>
						<td>${member.memberId}</td>
						<td>${member.memberPwd}</td>
						<td>${member.memberName}</td>
						<td>${member.memberEmail}</td>
						<td>${member.phone}</td>
						<td>${member.address}</td>
						<td>${member.birthday}</td>
						<td>${member.gender}</td>
						<td>${member.joinDate}</td>
					</tr>
				</c:forEach>

			</table>
		</section>

		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>