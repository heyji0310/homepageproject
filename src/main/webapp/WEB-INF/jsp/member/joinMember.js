window.onload = () => {
	const register = document.querySelector("#register");
	const memberId = document.querySelector("#memberId");
	const memberPwd = document.querySelector("#memberPwd");
	const memberName = document.querySelector("#memberName");
	const memberEmail = document.querySelector("#memberEmail");
	const phone = document.querySelector("#phone");
	const address = document.querySelector("#address");
	const birthday = document.querySelector("#birthday");
	
/*	
	const registerForm = document.querySelector("#registerForm");
	const req = {}
	new FormData(registerForm).forEach((value, key) => req[key] = value);
	console.log(req);
*/	
	register.addEventListener("click", async e => {
		e.preventDefault();
		const reqJSON = {
			memberId: memberId.value
			, memberPwd : memberPwd.value
			, memberName : memberName.value
			, memberEmail : memberEmail.value
			, phone : phone.value
			, address : address.value
			, birthday : birthday.value
			, gender : document.querySelector("input[name=gender]:checked").value
		};
		
		const response = await fetch(`/homepageProject/member/insert?${new URLSearchParams(reqJSON).toString()}`);
		
		const json = await response.json();
		console.log(json);
		alert(json.message);
		if (json.status) {
			location.href = json.url;
		}
	})
}