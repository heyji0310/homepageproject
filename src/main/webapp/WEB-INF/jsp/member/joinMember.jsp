<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>회원가입을 환영합니다.</title>
<script type="text/javascript" src="./joinMember.js"></script>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

</head>
<body>
	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>
		
		<section id="contents">

<div>
		<h1><span id="title">회원 가입</span></h1>
	</div>
	<!-- form name="joinMemberForm" action="/homepageProject/member/insert" method="post" enctype="utf-8"-->
	<form id="registerForm">
 	  <div id="box-outline" align="center" style="width:360px" style="height:180px">
		<div id="box-inline" align="right" style="margin: 30px">

			<label for="memberId">아이디 *</label> 
			<input type="text" id="memberId" name="memberId" autofocus="autofocus" placeholder="영문, 숫자 5자 이상 입력해주세요." />	
		<br/>		

			<label for="memberPwd">비밀번호 *</label> 
			<input type="password" id="memberPwd" name="memberPwd" autocomplete="off" placeholder="6자 이상 입력해주세요."/>
		<br/>

			<label for="memberName">이름 *</label> 
			<input type="text" id="memberName" name="memberName" autocomplete="off"/>
		<br/>

			<label for="memberEmail">이메일 *</label> 
			<input type="text" id="memberEmail" name="memberEmail" autocomplete="off"/>
		<br/>
		
			<label for="phone">휴대폰 번호 *</label> 
			<input type="tel" id="phone" name="phone" autocomplete="off" maxlength="11" placeholder="숫자만 입력해주세요."/>
			
		<br/>

			<label for="address">주소 *</label> 
			<input type="text" id="address" name="address" autocomplete="off"/>
		<br/>

			<label for="birthday">생년월일 *</label> 
			<input type="date" id="birthday" name="birthday" autocomplete="off"/>
		<br/>

			<label for="gender">성별 *</label> 
			<input type="radio" id="gender_M" name="gender" value="M" autocomplete="off" checked/>
			<label for="gender_M">남성</label>
			<input type="radio" id="gender_F" name="gender" value="F" autocomplete="off"/>
			<label for="gender_F">여성</label>
		</div>
		</div>
		
		<div>
			<input type="checkbox" name="agree" value=false onclick="selectAll();"><b>모두 동의합니다.</b>
			<br />
			<input type="checkbox" id="agree1" name="agree1" value=false>(필수) 이용약관과 개인정보 수집 및 이용에 동의합니다.
			<br />
			<input type="checkbox" id="agree2" name="agree2" value=false>(필수) 만 14세 이상입니다.
			<br />
			<input type="checkbox" id="agree3" name="agree3" value=false>(선택) 이메일 및 SMS 마케팅 정보 수신에 동의합니다.
		</div>
		
		<input id="register" type="submit" value="가입하기">
		<input type="reset" value="다시입력">
		<a href="/homepageProject/homepageMain.do">돌아가기</a>
				
	</form>
<script>
	function selectAll(){
		if('agree' == true) {
			document.getElementById('agree1').value = true;
			document.getElementById('agree2').value = true;
			document.getElementById('agree3').value = true;
		}
	}

</script>
		</section>
		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>

</body>
</html>