<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>비밀번호 찾기</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

</head>
<body>

	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>
	
		<section id="contents">


		
	<div id="findPwd" class="findPwd">
		<span class="title">비밀번호 찾기</span>
	</div>
	<form action="<c:url value='/member/findPwd'/>" name="findPwd" method="post" enctype="utf-8">
		<label for="memberId" class="title">아이디</label> 
		<input type="text" id="memberId" name="memberId" class="designSettingElement shape" autofocus="autofocus">

		<div class="row">
			<label for="phone" class="title">연락처</label> 
			<input type="text" id="phone" name="phone" class="designSettingElement shape">
		</div>

		<div id="findPasswordErrorMsg" class="error-msg"></div>

		<div class="btn-wrapper">
			<button type="submit">비밀번호 찾기</button>
			<a class="login" href="<c:url value='/member/loginForm.do'/>">로그인 하러가기</a>
		</div>

	</form>
	</section>
	
	<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>