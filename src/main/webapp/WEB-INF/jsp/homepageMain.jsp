<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>혜지의 공간</title>
<jsp:include page="/jsp/homepage/includes/css.jsp" flush="true"/>

<link rel="stylesheet"
	href="<c:url value='/jsp/homepage/css/mainpage.css?after'/>">
<script src="https://kit.fontawesome.com/a1cdd9b3c8.js"
	crossorigin="anonymous"></script>

<style>
body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>")
		no-repeat;
	background-size: 100%;
}
</style>
</head>
<body>
	<div id="container">
		<%@ include file="/jsp/homepage/includes/commonPagesTop.jsp"%>
		
		<%@ include file="/jsp/homepage/includes/commonPagesMainSide.jsp"%>
		
		<section id="contents">
		
			<h1 style="text-align: center;" style="margin: 100px">WellCome! :)</h1>
	
	
			<div class="button-width" align="center" style="margin: 30px">
				<a class="w-btn-outline w-btn-indigo-outline" href="/homepageProject/member/loginForm.do">로그인하기</a>
	
				<a class="w-btn-outline w-btn-indigo-outline" style="margin: 30px;"
					style="float:left" id="register"
					href="<c:url value='/member/joinMemberForm.do'/>">회원가입</a>
			</div>

		</section>

		<%@ include file="/jsp/homepage/includes/commonPagesFooter.jsp"%>
	</div>
</body>
</html>