<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>매너있는 채팅~!</title>
<link rel="stylesheet" href="<c:url value='/jsp/homepage/css/mainpage.css?after'/>" >
<script src="https://kit.fontawesome.com/a1cdd9b3c8.js" crossorigin="anonymous"></script>

<style>
table, th, td {
	border: 1px dotted #ccc;
	border-collapse: collapse;
}

th, td {
	padding: 10px 20px;
}

body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>")
		no-repeat;
	background-size: 100%;
}
</style>
</head>

</head>
<body>



	<div id="container">
		<header id="header" style="text-align: center;">
			<img alt="logo" src="<c:url value='/jsp/homepage/images/logo.png'/>"
				width="15%">
		</header>
		
		
		<aside id="sidebar">
			<h1>MENU</h1>

			<nav>
				<ul id="homeMenu">
					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="HOME"
						href="<c:url value='/homepageSub.jsp'/>"><i class="fa-solid fa-house-chimney" style="color:#0056ff"></i>HOME</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="MY_PAGE"
						href="<c:url value='/jsp/member/view.jsp'/>">MY_PAGE</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="CHATTING"
						href="<c:url value='/jsp/chatting/chattingRoom.jsp'/>">CHATTING</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now">
					<a class="menu-name" alt="STORYBOARD" 
						href="<c:url value='/storyboard/postList'/>">STORYBOARD</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>
				</ul>
			</nav>
		</aside>
		
		
		<section id="contents">

<h1>채팅룸</h1>

</section>
		
		
				
		<footer id="footer">
			<h4>제작자:안혜지</h4>
		</footer>
		
	</div>
</body>
</html>