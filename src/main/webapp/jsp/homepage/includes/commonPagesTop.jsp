<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="https://kit.fontawesome.com/a1cdd9b3c8.js" crossorigin="anonymous"></script>

<style>
table, th, td {
	border: 1px dotted #ccc;
	border-collapse: collapse;
}

th, td {
	padding: 10px 20px;
}

body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>") no-repeat;
	background-size: 100%; 
}
</style>

	<header id="header" style="text-align: center;">
		<a href="<c:url value='/homepageMain.do'/>"> <img alt="logo"
			src="<c:url value='/jsp/homepage/images/logo.png'/>" width="15%">
		</a>
	</header>
	
	
	
	