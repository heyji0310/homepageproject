<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<script src="https://kit.fontawesome.com/a1cdd9b3c8.js" crossorigin="anonymous"></script>

<style>
table, th, td {
	border: 1px dotted #ccc;
	border-collapse: collapse;
}

th, td {
	padding: 10px 20px;
}

body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>") no-repeat;
	background-size: 100%; 
}
</style>


		<aside id="sidebar">
			<h1>MENU</h1>

			<nav>
				<ul id="homeMenu">
					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="HOME"
						href="<c:url value='/homepageSub.do'/>"><i class="fa-solid fa-house-chimney" style="color:#0056ff"></i>HOME</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="MY_PAGE"
						href="<c:url value='/member/view.do'/>">MY_PAGE</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now"><a
						class="menu-name" alt="CHATTING"
						href="<c:url value='/chatting/chattingRoom.do'/>">CHATTING</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>


					<li class="menu-navi menu-main pageMenu now">
					<a class="menu-name" alt="STORYBOARD" 
						href="<c:url value='/storyBoard/postList.do'/>">STORYBOARD</a>
						<div class="menu-opener"></div>
						<div class="subMenuNaviListDiv">
							<ul class="subMenuNaviList"></ul>
						</div></li>
				</ul>
			</nav>
			
		</aside>


