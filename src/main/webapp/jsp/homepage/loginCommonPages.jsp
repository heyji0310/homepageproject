<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<link rel="stylesheet"
	href="<c:url value='/jsp/homepage/css/mainpage.css?after'/>">
<script src="https://kit.fontawesome.com/a1cdd9b3c8.js"
	crossorigin="anonymous"></script>

<style>
body {
	background: url("<c:url value='/jsp/homepage/images/home.jpg'/>")
		no-repeat;
	background-size: 100%;
}
</style>

<div id="container">
	<header id="header" style="text-align: center;">
		<a href="<c:url value='/homepageMain.jsp'/>"> <img alt="logo"
			src="<c:url value='/jsp/homepage/images/logo.png'/>" width="15%">
		</a>
	</header>


	<aside id="sidebar">
		<h1>MENU</h1>

		<nav>
			<ul id="homeMenu">
				<li class="menu-navi menu-main pageMenu now"><a
					class="menu-name" alt="HOME"
					href="<c:url value='/homepageMain.jsp'/>"><i
						class="fa-solid fa-house-chimney" style="color: #0056ff"></i>HOME</a>
					<div class="menu-opener"></div>
					<div class="subMenuNaviListDiv">
						<ul class="subMenuNaviList"></ul>
					</div></li>
			</ul>
		</nav>
	</aside>


	<section id="contents"></section>
	<footer id="footer">
		<h4>제작자:안혜지</h4>
	</footer>
</div>
