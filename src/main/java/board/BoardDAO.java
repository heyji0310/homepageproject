package board;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class BoardDAO {
	Connection conn;
	PreparedStatement pstmt;
	Statement stmt;
	DataSource dataFactory;
	ResultSet rs;
	
	public BoardDAO() {
		try {
			Context context = new InitialContext();
			Context envContext = (Context) context.lookup("java:/comp/env");
			dataFactory = (DataSource) envContext.lookup("jdbc/homepageProjectDB");
			conn = dataFactory.getConnection();
			conn.setAutoCommit(false);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

// 게시글 목록	
	public List<BoardBean> listBoards() {
		List<BoardBean> boardList = new ArrayList<>();
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from homepageboard";
			System.out.println("prepareStatememt: " + query);
			
			pstmt = conn.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				BoardBean listBoards = new BoardBean(
						
						rs.getInt("boardNo"),	
						rs.getString("postTitle"),	
						rs.getString("postContent"),
						rs.getString("postWriter"),
						rs.getTimestamp("dateCreate"),
						rs.getTimestamp("dateUpdate"),
						rs.getInt("viewsCount"),	
						rs.getInt("likesCount"),	
						rs.getString("isNotice"));
				System.out.println(listBoards);
				boardList.add(listBoards);
			}
			rs.close();
			pstmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return boardList;
	}

// 게시글 상세보기	
	public BoardBean postDetail(int boardNo) {
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from homepageboard where boardNo = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, boardNo);
			ResultSet rs = pstmt.executeQuery();
			BoardBean boardBean = null;
			
			if (rs.next()) {
				boardBean = new BoardBean(
						rs.getInt("boardNo"),	
						rs.getString("postTitle"),	
						rs.getString("postContent"),
						rs.getString("postWriter"),
						rs.getTimestamp("dateCreate"),
						rs.getTimestamp("dateUpdate"),
						rs.getInt("viewsCount"),
						rs.getInt("likesCount"),
						rs.getString("isNotice"));
			}
			rs.close();
			
			return boardBean;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
		return null;		
	}

// 게시글 등록	
	public int insertPost(BoardBean boardBean) throws SQLException{
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "insert into homepageboard (postTitle, postContent, postWriter)"
					+ "values (?,?,?)";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, boardBean.getPostTitle());
			pstmt.setString(2, boardBean.getPostContent());
			pstmt.setString(3, boardBean.getPostWriter());

			pstmt.executeUpdate();
			
			query = "SELECT LAST_INSERT_ID()";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			int boardNo = 0;
			if(rs.next()) {
				boardNo = rs.getInt(1);
			}
		
			return boardNo;
			
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
	}

// 게시글 수정	
	public int updatePost(BoardBean boardBean) throws SQLException{
		try {
			// connDB();
			conn = dataFactory.getConnection();
			
			String query = "UPDATE homepageboard SET postTitle = ?, postContent = ? WHERE boardNo = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, boardBean.getPostTitle());
			pstmt.setString(2, boardBean.getPostContent());
			pstmt.setInt(3, boardBean.getBoardNo());
			
			System.out.println(boardBean);

			return pstmt.executeUpdate();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}		
	}
	
	
// 게시글 삭제	
	public void deletePost(int boardNo) {
		try {
			conn = dataFactory.getConnection();
			
			stmt = conn.createStatement();
			String query = "delete from homepageboard where boardNo=?";
			System.out.println("prepareStatememt:" + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, boardNo);
			pstmt.executeUpdate();
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void close() throws Exception {
		if (conn != null) {
			conn.close();
		}
	}

}
