package board;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BoardBean {
	private int boardNo;
	private String postTitle;
	private String postContent;
	private String postWriter;
	private Date dateCreate;
	private Date dateUpdate;
	private int viewsCount;
	private int likesCount;
	private String isNotice;
	
}
