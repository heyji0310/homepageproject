package board;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class BoardFileDAO {
	Connection conn;
	PreparedStatement pstmt;
	Statement stmt;
	DataSource dataFactory;
	ResultSet rs;
	
	public BoardFileDAO() {
		try {
			Context context = new InitialContext();
			Context envContext = (Context) context.lookup("java:/comp/env");
			dataFactory = (DataSource) envContext.lookup("jdbc/homepageProjectDB");
			conn = dataFactory.getConnection();
			conn.setAutoCommit(false);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

// 게시글에 속한 첨부파일 목록	
	public List<BoardFileBean> list(int boardNo) {
		List<BoardFileBean> list = new ArrayList<>();
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from board_file where boardNo = ?";
			System.out.println("prepareStatememt: " + query);
			
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, boardNo);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				BoardFileBean BoardFileBean = new BoardFileBean(
						
						rs.getInt("f_id"),	
						rs.getInt("boardNo"),	
						rs.getString("org_name"),
						rs.getString("real_name"),
						rs.getString("content_type"),
						rs.getLong("length"));
				System.out.println(BoardFileBean);
				list.add(BoardFileBean);
			}
			rs.close();
			pstmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

// 게시글 상세보기	
	public BoardBean postDetail(int boardNo) {
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from homepageboard where boardNo = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, boardNo);
			ResultSet rs = pstmt.executeQuery();
			BoardBean boardBean = null;
			
			if (rs.next()) {
				boardBean = new BoardBean(
						rs.getInt("boardNo"),	
						rs.getString("postTitle"),	
						rs.getString("postContent"),
						rs.getString("postWriter"),
						rs.getTimestamp("dateCreate"),
						rs.getTimestamp("dateUpdate"),
						rs.getInt("viewsCount"),
						rs.getInt("likesCount"),
						rs.getString("isNotice"));
			}
			rs.close();
			
			return boardBean;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
		return null;		
	}

// 게시글 등록	
	public void insertPostFile(BoardFileBean boardFileBean) throws SQLException{
		try {
			// connDB();
			System.out.println("파일이 들어올까");
			conn = dataFactory.getConnection();
			String query = "insert into board_file (boardNo, org_name, real_name, content_type, length)"
					+ " values (?,?,?,?,?)";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, boardFileBean.getBoardNo());
			pstmt.setString(2, boardFileBean.getOrg_name());
			pstmt.setString(3, boardFileBean.getReal_name());
			pstmt.setString(4, boardFileBean.getContent_type());			
			pstmt.setLong(5, boardFileBean.getLength());			

			pstmt.executeUpdate();
			
			
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
	}
	
	

// 게시글 수정	
	public int updatePost(BoardBean boardBean) throws SQLException{
		try {
			// connDB();
			conn = dataFactory.getConnection();
			
			String query = "UPDATE homepageboard SET postTitle = ?, postContent = ? WHERE boardNo = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, boardBean.getPostTitle());
			pstmt.setString(2, boardBean.getPostContent());
			pstmt.setInt(3, boardBean.getBoardNo());
			
			System.out.println(boardBean);

			return pstmt.executeUpdate();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}		
	}
	
	
// 게시글 삭제	
	public void deletePost(int boardNo) {
		try {
			conn = dataFactory.getConnection();
			
			stmt = conn.createStatement();
			String query = "delete from homepageboard where boardNo=?";
			System.out.println("prepareStatememt:" + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, boardNo);
			pstmt.executeUpdate();
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void close() throws Exception {
		if (conn != null) {
			conn.close();
		}
	}

}
