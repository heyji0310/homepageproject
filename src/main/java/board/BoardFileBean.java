package board;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BoardFileBean {
	private int f_id;
	private int boardNo;
	private String org_name;
	private String real_name;
	private String content_type;
	private long length;
}
