package member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class MemberDAO {
	Connection conn;
	PreparedStatement pstmt;
	Statement stmt;
	DataSource dataFactory;
	
	public MemberDAO() {
		try {
			Context context = new InitialContext();
			Context envContext = (Context) context.lookup("java:/comp/env");
			dataFactory = (DataSource) envContext.lookup("jdbc/homepageProjectDB");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<MemberBean> listMembers() {
		List<MemberBean> list = new ArrayList<>();
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from homepagemember ";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				MemberBean member = new MemberBean(
						
						rs.getString("memberId"),	
						rs.getString("memberPwd"),	
						rs.getString("memberName"),	
						rs.getString("memberEmail"),
						rs.getString("phone"),
						rs.getString("address"),	
						rs.getString("birthday"),	
						rs.getString("gender"),	
						rs.getString("isAdmin"),
						rs.getDate("joinDate"));
				System.out.println(member);
				list.add(member);
			}
			rs.close();
			pstmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
//	public boolean loginMem(MemberBean member) {
//		try {
//			Connection con = dataFactory.getConnection();
//			String query = "select * from homepagemember where memberId = ? and memberPwd = ?";
//			query += " (memberId, memberPwd)";
//			query += " values(?,?)";
//			System.out.println("prepareStatememt: " + query);
//			pstmt = con.prepareStatement(query);
//						
//			pstmt.setString(1, member.getMemberId());
//			pstmt.setString(2, member.getMemberPwd());
//			
//			ResultSet result = pstmt.executeQuery(); // 성공 1, 실패 -1
//			pstmt.close();
//			if(result > 0) return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return false;
//	}

//	public void addMember(MemberBean member) {
//		try {
//			Connection con = dataFactory.getConnection();
//			String query = "insert into homepagemember";
//			query += " (memberId, memberPwd, memberName, memberEmail, phone, address, birthday, gender)";
//			query += " values(?,?,?,?,?, ?,?,?)";
//			System.out.println("prepareStatememt: " + query);
//			pstmt = con.prepareStatement(query);
//						
//			pstmt.setString(1, member.getMemberId());
//			pstmt.setString(2, member.getMemberPwd());
//			pstmt.setString(3, member.getMemberName());
//			pstmt.setString(4, member.getMemberEmail());
//			pstmt.setString(5, member.getPhone());
//			pstmt.setString(6, member.getAddress());
//			pstmt.setString(7, member.getBirthday());
//			pstmt.setString(8, member.getGender());
//			pstmt.executeUpdate();
//			pstmt.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	public MemberBean viewMember(String memberId) {
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from homepagemember where memberId = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, memberId);
			ResultSet rs = pstmt.executeQuery();
			MemberBean memberBean = null;
			
			if (rs.next()) {
				memberBean = new MemberBean(
						rs.getString("memberId"),	
						rs.getString("memberPwd"),	
						rs.getString("memberName"),	
						rs.getString("memberEmail"),
						rs.getString("phone"),
						rs.getString("address"),
						rs.getString("birthday"),
						rs.getString("gender"),
						rs.getString("isAdmin"),
						rs.getDate("joinDate"));
			}
			rs.close();
			
			return memberBean;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
		return null;		
	}

	public int insertMember(MemberBean memberBean) throws SQLException{
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "insert into homepagemember (memberId, memberPwd, memberName, memberEmail, phone, address, birthday, gender) "
					+ "values (?,?,?,?,?, ?,?,?)";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, memberBean.getMemberId());
			pstmt.setString(2, memberBean.getMemberPwd());
			pstmt.setString(3, memberBean.getMemberName());
			pstmt.setString(4, memberBean.getMemberEmail());
			pstmt.setString(5, memberBean.getPhone());
			pstmt.setString(6, memberBean.getAddress());
			pstmt.setString(7, memberBean.getBirthday());
			pstmt.setString(8, memberBean.getGender());

			return pstmt.executeUpdate();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			throw e; //해당 함수를 호출한 부분으로 예외를 던진다 
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}		
	}
	
	public int updateMember(MemberBean memberBean) throws SQLException{
		try {
			// connDB();
			conn = dataFactory.getConnection();
			
			String query = "update homepagemember set memberPwd =?, memberName =?, memberEmail =?, phone =?, address =?, birthday =?, gender =? where memberId = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, memberBean.getMemberPwd());
			pstmt.setString(2, memberBean.getMemberName());
			pstmt.setString(3, memberBean.getMemberEmail());
			pstmt.setString(4, memberBean.getPhone());
			pstmt.setString(5, memberBean.getAddress());
			pstmt.setString(6, memberBean.getBirthday());
			pstmt.setString(7, memberBean.getGender());
			pstmt.setString(8, memberBean.getMemberId());
			
			System.out.println(memberBean);

			return pstmt.executeUpdate();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			throw e; //해당 함수를 호출한 부분으로 예외를 던진다 
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}		
	}
	
	public boolean withdrawalMember(MemberBean memberBean) {
		try {
			// connDB();
			conn = dataFactory.getConnection();
						
			String query = "update homepagemember set isUsed ='N' where memberId = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, memberBean.getMemberId());
			return pstmt.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
		return false;
	}
	
//	public boolean isExisted(MemberBean memberBean) {
//		boolean result = false;
//		String memberId = memberBean.getMemberId();
//		String memberPwd = memberBean.getMemberPwd();
//		try {
//			conn = dataFactory.getConnection();
//			String query = "select case count(*) when 1 then 'true' else 'false' end as result from homepagemember";
//			query += " where memberId=? and memberPwd=?";
//			System.out.println(query);
//			System.out.println(memberId);
//			System.out.println(memberPwd);
//			pstmt = conn.prepareStatement(query);
//			pstmt.setString(1, memberId);
//			pstmt.setString(2, memberPwd);
//			ResultSet rs = pstmt.executeQuery();
//			rs.next(); 
//			result = Boolean.parseBoolean(rs.getString("result"));
//			System.out.println("result=" + result);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
	
	public void delMember(String memberId) {
		try {
			conn = dataFactory.getConnection();
			stmt = conn.createStatement();
			String query = "delete from homepagemember" + " where id=?";
			System.out.println("prepareStatememt:" + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, memberId);
			pstmt.executeUpdate();
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public MemberBean findId(String memberName, String memberEmail) {
		MemberBean memberBean = null;
		try {
			// connDB();
			conn = dataFactory.getConnection();
			String query = "select * from homepagemember where memberName = ? and memberEmail = ?";
			System.out.println("prepareStatememt: " + query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, memberName);
			pstmt.setString(2, memberEmail);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				memberBean = new MemberBean(
						rs.getString("memberId"),	
						rs.getString("memberPwd"),	
						rs.getString("memberName"),	
						rs.getString("memberEmail"),
						rs.getString("phone"),
						rs.getString("address"),
						rs.getString("birthday"),
						rs.getString("gender"),
						rs.getString("isAdmin"),
						rs.getDate("joinDate"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {}
		}
		return memberBean;		
	}

	public MemberBean findPwd(String memberId, String phone) {
		MemberBean memberBean = null;
		try {
		// connDB();
			conn = dataFactory.getConnection();
			String queryPwd = "select * from homepagemember where memberId = ? and phone = ?";
			System.out.println("prepareStatememt: " + queryPwd);
			pstmt = conn.prepareStatement(queryPwd);
			pstmt.setString(1, memberId);
			pstmt.setString(2, phone);
			ResultSet rs = pstmt.executeQuery();
			
				
			if (rs.next()) {
				memberBean = new MemberBean(
						rs.getString("memberId"),	
						rs.getString("memberPwd"),	
						rs.getString("memberName"),	
						rs.getString("memberEmail"),
						rs.getString("phone"),
						rs.getString("address"),
						rs.getString("birthday"),
						rs.getString("gender"),
						rs.getString("isAdmin"),
						rs.getDate("joinDate"));
			}
				rs.close();
					
			} catch (Exception e) {
				e.printStackTrace();
		} finally {
				try {
					pstmt.close();
					conn.close();
				} catch (Exception e) {}
			}
			return memberBean;
	}
}
