package member;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberBean {
	private String memberId;
	private String memberPwd;
	private String memberName;
	private String memberEmail;
	private String phone;
	private String address;
	private String birthday;
	private String gender;
	private String isAdmin;
	private Date joinDate;
	
}
