package member;
//package homepage;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.sql.Date;
//import java.sql.SQLException;
//
//import org.json.JSONObject;
//
//import jakarta.servlet.RequestDispatcher;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import jakarta.servlet.http.HttpSession;
//
///**
// * Servlet implementation class MemberServlet
// */
//
//@WebServlet("/member/*")
//public class MemberServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//       
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public MemberServlet() {
//        super();
//        // TODO Auto-generated constructor stub
//    }
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		MemberDAO memberDAO;
//		MemberBean memberBean;
//		JSONObject jsonResult;
//		PrintWriter printWriterOut;
//		HttpSession session;
//		
//		System.out.println("MemberSevlet============ : " + request.getRequestURI());
//		if (request.getRequestURI().equals("/homepageProject/jsp/member/view")) {
//			
//			String memberId = request.getParameter("loginId");
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.viewMember(memberId);
//			if(memberBean.getMemberId().equals(memberId)) {
//				request.setAttribute("memberBean", memberBean);
//				RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/view.jsp");
//				dispatch.forward(request, response);
//			} 
//			
//		} 
//		
////		if (request.getRequestURI().equals("/homepageProject/member/login")) {
////			memberDAO = new MemberDAO();
////			String memberId = request.getParameter("loginId");
////			String memberPwd = request.getParameter("loginPwd");
////			memberBean = memberDAO.viewMember(memberId);
////			if(memberBean != null && memberBean.getMemberPwd().equals(memberPwd)) {
////				request.getSession().setAttribute("sess_memberId", "sess_memberPwd");
////				RequestDispatcher dispatch = request.getRequestDispatcher("/homepageSub.jsp");
////				dispatch.forward(request, response);
////			} else {
////				RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/login.jsp");
////				dispatch.forward(request, response);
////			}
////			
////		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/login")) {
//			response.setContentType("text/html;charset=utf-8");
//			memberDAO = new MemberDAO();
//			printWriterOut = response.getWriter();
//			session = request.getSession();
//			String loginId = request.getParameter("loginId");
//			String loginPwd = request.getParameter("loginPwd");
//			memberBean = memberDAO.viewMember(loginId);
//			
//			System.out.println("login : " + memberBean);
//			
//				if(memberBean != null && memberBean.getMemberPwd().equals(loginPwd)) {
//					session.setAttribute("sess_memberBean", memberBean);
//					response.sendRedirect("/homepageProject/homepageSub.jsp");
//					printWriterOut.print("안녕하세요 " + loginId + "님!!!");
//				} else {
//					RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/login.jsp");
//					dispatch.forward(request, response);
//					printWriterOut.print("<a href='/homepageProject/jsp/member/login.jsp'>다시 로그인 하세요!!</a>");
//				}
//		}
//		
////		if (request.getRequestURI().equals("/homepageProject/member/withdrawalMember")) {
////			response.setContentType("text/html;charset=utf-8");
////			memberBean = (MemberBean) request.getSession().getAttribute("sess_memberBean");
////			memberDAO = new MemberDAO();
////			boolean flag = memberDAO.withdrawalMember(memberBean);
////			if (flag) {
////				
////			} else {
////				
////			}
////			printWriterOut = response.getWriter();
////		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/withdrawalMember")) {
//			response.setContentType("text/html;charset=utf-8");
//			memberBean = (MemberBean) request.getSession().getAttribute("sess_memberBean");
//			memberDAO = new MemberDAO();
//			jsonResult = new JSONObject();
//			
//			boolean flag = memberDAO.withdrawalMember(memberBean);
//			if (flag) {
//				jsonResult.put("message", "탈퇴가 완료되었습니다.");
//				jsonResult.put("url", "/homepageProject/homepageMain.jsp");
//			} else {
//				
//			}
//			printWriterOut = response.getWriter();
//			printWriterOut.println(jsonResult.toString());
//			
//		}
//		
//		
//		if (request.getRequestURI().equals("/homepageProject/member/list")) {
//			memberDAO = new MemberDAO();
//			request.setAttribute("listMembers", memberDAO.listMembers());
//
//			RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/list.jsp");
//			dispatch.forward(request, response);
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/dupUidCheck")) {
//			String memberId = request.getParameter("memberId");
//			
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.viewMember(memberId);
//			jsonResult = new JSONObject();
//			printWriterOut = response.getWriter();
//			
//			if (memberBean == null) {
//				jsonResult.put("status", true);
//				jsonResult.put("message", "해당 아이디는 사용 가능합니다.");
//			} else {
//				jsonResult.put("status", false);
//				jsonResult.put("message", "해당 아이디는 이미 사용중 입니다.");
//			}
//			printWriterOut = response.getWriter();
//			printWriterOut.println(jsonResult.toString());
//			
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/findId")) {
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.findId(memberName, memberEmail);
//
//			if (memberBean == null) {
//				request.setAttribute("message", "검색 결과가 없습니다.");
//			} else {
//				request.setAttribute("message", "아이디는 " + memberBean.getMemberId() + "입니다.");
//			}
//			RequestDispatcher dispatchId = request.getRequestDispatcher("/jsp/member/findIdResurt.jsp");
//			dispatchId.forward(request, response);
//			
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/findPwd")) {
//			String memberId = request.getParameter("memberId");
//			String phone = request.getParameter("phone");
//			
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.findPwd(memberId, phone);
//			
//			if (memberBean == null) {
//				request.setAttribute("message", "검색 결과가 없습니다.");
//			} else {
//				request.setAttribute("message", "비밀번호는 " + memberBean.getMemberPwd() + "입니다.");
//			}
//			RequestDispatcher dispatchPwd = request.getRequestDispatcher("/jsp/member/findPwdResurt.jsp");
//			dispatchPwd.forward(request, response);
//				
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/insert")) {
//			String memberId = request.getParameter("memberId");
//			String memberPwd = request.getParameter("memberPwd");
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			String phone = request.getParameter("phone");
//			String address = request.getParameter("address");
//			String birthday = request.getParameter("birthday");
//			String gender = request.getParameter("gender");
//			
//			
//			jsonResult = new JSONObject();
//			
//			memberDAO = new MemberDAO();
//			printWriterOut = response.getWriter();
//			try {
//				memberBean = MemberBean.builder()
//					.memberId(memberId)
//					.memberPwd(memberPwd)
//					.memberName(memberName)
//					.memberEmail(memberEmail)
//					.phone(phone)
//					.address(address)
//					.birthday(birthday)
//					.gender(gender)
//					.build();
//				memberDAO.insertMember(memberBean);
//System.out.println("insert: " + memberBean);
//			} catch (SQLException e) {
//				e.printStackTrace();
//				jsonResult.put("status", false);
//				jsonResult.put("message", "해당아이디는 사용하실 수 없습니다.");
//				printWriterOut.print(jsonResult);
//				return;
//			}
//			
//			
////			BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
////			String jsonStr = in.readLine();
////			System.out.println("jsonStr = " + jsonStr);
////			JSONObject jsonMember = new JSONObject(jsonStr);
//			
//			jsonResult.put("status", true);
//			jsonResult.put("url", "/homepageProject/homepageMain.jsp");
//			jsonResult.put("message", "가입을 환영합니다.");
//			
//			
//			printWriterOut.print(jsonResult);
//		}
//
//		if (request.getRequestURI().equals("/homepageProject/member/updateMemberInfo")) {
//			
//			System.out.println("heheheheheh");
//			memberBean = (MemberBean) request.getSession().getAttribute("sess_memberBean");
//			String memberId = memberBean.getMemberId();
//			String memberPwd = request.getParameter("memberPwd");
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			String phone = request.getParameter("phone");
//			String address = request.getParameter("address");
//			String birthday = request.getParameter("birthday");
//			String gender = request.getParameter("gender");
//			Date joinDate = memberBean.getJoinDate();
//			
//			System.out.println("update: " + memberPwd + " " + memberName + " " + memberName + " " + memberEmail + " " +  phone+ " " + address + " " + birthday + " " + gender);
//			
//			jsonResult = new JSONObject();
//			
//			memberDAO = new MemberDAO();
//			printWriterOut = response.getWriter();
//			try {
//				MemberBean updateMember = MemberBean.builder()
//					.memberId(memberId)
//					.memberPwd(memberPwd)
//					.memberName(memberName)
//					.memberEmail(memberEmail)
//					.phone(phone)
//					.address(address)
//					.birthday(birthday)
//					.gender(gender)
//					.joinDate(joinDate)
//					.build();
//				System.out.println("update: " + memberDAO.updateMember(updateMember));
//				request.getSession().setAttribute("sess_memberBean", updateMember);
//				
//			} catch (SQLException e) {
//				e.printStackTrace();
//				jsonResult.put("status", false);
//				jsonResult.put("message", "아이디를 찾을 수 없습니다.");
//				printWriterOut.print(jsonResult);
//				return;
//			}
//			
//			jsonResult.put("status", true);
//			jsonResult.put("url", "/homepageProject/jsp/member/view.jsp");
//			jsonResult.put("message", "수정이 완료되었습니다..");
//			
//			
//			printWriterOut.print(jsonResult);
//		}
//		
//	}
//
//	
//	
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}
//	
////	protected void doHandle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
////		request.setCharacterEncoding("utf-8");
////		response.setContentType("text/html;charset=utf-8");
////		PrintWriter out = response.getWriter();
////		HttpSession session = request.getSession();
////		String memberId = request.getParameter("memberId");
////		String memberPwd = request.getParameter("memberPwd");
////		if (session.isNew()){
////			if(memberId != null){
////				session.setAttribute("memberId", memberId);
////				String url = response.encodeURL("login");
////				out.println("<a href="+url+">로그인 상태 확인</a>");
////			}else {
////				out.print("<a href='login2.html'>다시 로그인 하세요!!</a>");
////				session.invalidate();
////			}
////		}else{
////			memberId = (String) session.getAttribute("user_id");
////			if (memberId != null && memberId.length() != 0) {
////				out.print("안녕하세요 " + memberId + "님!!!");
////			} else {
////				out.print("<a href=\"<c:url value='/jsp/member/login.jsp'/>\">다시 로그인 하세요!!</a>");
////				session.invalidate();
////			}
////		}
////	}
//	
//	
//}
//
////@WebServlet("/sess") 
////public class LoginSession extends HttpServlet{
////	protected void doGet(HttpServletRequest request, HttpServletResponse response)
////	throws ServletException, IOException {
////		response.setContentType("text/html;charset=utf-8");
////		PrintWriter out = response.getWriter();
////		HttpSession session = request.getSession();
////		out.println("세션 아이디: " + session.getId() + "<br>");
////		out.println("최초 세션 생성 시각: " + new Date(session.getCreationTime()) + "<br>");
////		out.println("최근 세션 접근 시각: " + new Date(session.getLastAccessedTime()) + "<br>");
////		out.println("최근 세션 유효 시간: " + session.getMaxInactiveInterval() + "<br>");
////		if(session.isNew()) {
////			out.print("새 세션이 만들어졌습니다.");
////		}
////	}
////}
