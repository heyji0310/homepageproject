package action;

import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import member.MemberBean;
import member.MemberDAO;

public class MemberAction {

	public String loginForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("login 시도");
		return "/jsp/member/login.jsp";
	}

	public JSONObject login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setContentType("application/json;charset=utf-8");
		JSONObject jsonIn = new JSONObject(request.getReader().readLine());
		
		MemberDAO memberDAO = new MemberDAO();
		HttpSession session = request.getSession();
		String loginId = jsonIn.getString("loginId");
		String loginPwd = jsonIn.getString("loginPwd");
		MemberBean memberBean = memberDAO.viewMember(loginId);

		System.out.println("login : " + memberBean);
		JSONObject jsonOut = new JSONObject();
		if (memberBean != null && memberBean.getMemberPwd().equals(loginPwd)) {
			session.setAttribute("sess_memberBean", memberBean);
			jsonOut.put("status", true);
			jsonOut.put("message", "안녕하세요 " + loginId + "님!!!");
			jsonOut.put("url", "/homepageProject/homepageSub.do");
		} else {
			jsonOut.put("status", false);
			jsonOut.put("message", "다시 로그인 하세요!!");
		}
		return jsonOut;
	}

	public String joinMemberForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("회원가입 시도");
		return "/jsp/member/joinMember.jsp";
	}
	
	public String findIdForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		return "/jsp/member/findIdForm.jsp";
	}
	
	public String findIdResult(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		
		return "/jsp/member/findIdResurt.jsp";
	}
	
	public JSONObject findId(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		response.setContentType("application/json;charset=utf-8");
		JSONObject jsonIn = new JSONObject(request.getReader().readLine());
		
		MemberDAO memberDAO = new MemberDAO();
		HttpSession session = request.getSession();
		String memberName = jsonIn.getString("memberName");
		String memberEmail = jsonIn.getString("memberEmail");
		MemberBean memberBean = memberDAO.findId(memberName, memberEmail);

		System.out.println("findId : " + memberBean);
		JSONObject jsonOut = new JSONObject();
		if (memberBean != null) {
			jsonOut.put("status", true);
			jsonOut.put("message", "아이디는 " + memberBean.getMemberId() + " 입니다");
			jsonOut.put("url", "/homepageProject/homepageSub.do");
		} else {
			jsonOut.put("status", false);
			jsonOut.put("message", "찾으시는 정보가 올바르지 않습니다.");
		}
		return null;
		
//		if (request.getRequestURI().equals("/homepageProject/member/findId")) {
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			
//			MemberDAO memberDAO = new MemberDAO();
//			MemberBean memberBean = memberDAO.findId(memberName, memberEmail);
//
//			if (memberBean == null) {
//				request.setAttribute("message", "검색 결과가 없습니다.");
//			} else {
//				request.setAttribute("message", "아이디는 " + memberBean.getMemberId() + "입니다.");
//			}
//			RequestDispatcher dispatchId = request.getRequestDispatcher("/jsp/member/findIdResurt.jsp");
//			dispatchId.forward(request, response);
//			
//		}
//		return "/jsp/member/findIdResurt.jsp";
	}
	
	public String findPwdForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		return "/jsp/member/findPwdForm.jsp";
	}
	
	public String findPwdResult(HttpServletRequest request, HttpServletResponse response) throws Exception {

		return "/jsp/member/findPwdResurt.jsp";
	}
	
	public String findPwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (request.getRequestURI().equals("/homepageProject/member/findPwd")) {
			String memberId = request.getParameter("memberId");
			String phone = request.getParameter("phone");
			
			MemberDAO memberDAO = new MemberDAO();
			MemberBean memberBean = memberDAO.findPwd(memberId, phone);
			
			if (memberBean == null) {
				request.setAttribute("message", "검색 결과가 없습니다.");
			} else {
				request.setAttribute("message", "비밀번호는 " + memberBean.getMemberPwd() + "입니다.");
			}
				
		}
		return "/jsp/member/findPwdResurt.jsp";
	}
	
	public String view(HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (request.getRequestURI().equals("/homepageProject/jsp/member/view")) {
System.out.println("여기로 들어옵니까");
			String memberId = request.getParameter("loginId");
			MemberDAO memberDAO = new MemberDAO();
			MemberBean memberBean = memberDAO.viewMember(memberId);
			if(memberBean.getMemberId().equals(memberId)) {
				request.setAttribute("memberBean", memberBean);
				RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/view.jsp");
				dispatch.forward(request, response);
			} 
		} 	
		return "/jsp/member/view.jsp";
	}
	
	public void insert (HttpServletRequest request, HttpServletResponse response) throws Exception {
			String memberId = request.getParameter("memberId");
			String memberPwd = request.getParameter("memberPwd");
			String memberName = request.getParameter("memberName");
			String memberEmail = request.getParameter("memberEmail");
			String phone = request.getParameter("phone");
			String address = request.getParameter("address");
			String birthday = request.getParameter("birthday");
			String gender = request.getParameter("gender");
			
			
			JSONObject jsonResult = new JSONObject();
			
			MemberDAO memberDAO = new MemberDAO();
			PrintWriter printWriterOut = response.getWriter();
			try {
				MemberBean memberBean = MemberBean.builder()
					.memberId(memberId)
					.memberPwd(memberPwd)
					.memberName(memberName)
					.memberEmail(memberEmail)
					.phone(phone)
					.address(address)
					.birthday(birthday)
					.gender(gender)
					.build();
				memberDAO.insertMember(memberBean);
System.out.println("insert: " + memberBean);
			} catch (SQLException e) {
				e.printStackTrace();
				jsonResult.put("status", false);
				jsonResult.put("message", "해당아이디는 사용하실 수 없습니다.");
				printWriterOut.print(jsonResult);
				return;
			}
			
			jsonResult.put("status", true);
			jsonResult.put("url", "/homepageProject/homepageMain.jsp");
			jsonResult.put("message", "가입을 환영합니다.");
			
			
			printWriterOut.print(jsonResult);
			
		}
	

//	@WebServlet("/member/*")
//public class MemberServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//       
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public MemberServlet() {
//        super();
//        // TODO Auto-generated constructor stub
//    }
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		MemberDAO memberDAO;
//		MemberBean memberBean;
//		JSONObject jsonResult;
//		PrintWriter printWriterOut;
//		HttpSession session;
//		
//		System.out.println("MemberSevlet============ : " + request.getRequestURI());
//		if (request.getRequestURI().equals("/homepageProject/jsp/member/view")) {
//			
//			String memberId = request.getParameter("loginId");
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.viewMember(memberId);
//			if(memberBean.getMemberId().equals(memberId)) {
//				request.setAttribute("memberBean", memberBean);
//				RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/view.jsp");
//				dispatch.forward(request, response);
//			} 
//			
//		} 
//		
////		if (request.getRequestURI().equals("/homepageProject/member/login")) {
////			memberDAO = new MemberDAO();
////			String memberId = request.getParameter("loginId");
////			String memberPwd = request.getParameter("loginPwd");
////			memberBean = memberDAO.viewMember(memberId);
////			if(memberBean != null && memberBean.getMemberPwd().equals(memberPwd)) {
////				request.getSession().setAttribute("sess_memberId", "sess_memberPwd");
////				RequestDispatcher dispatch = request.getRequestDispatcher("/homepageSub.jsp");
////				dispatch.forward(request, response);
////			} else {
////				RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/login.jsp");
////				dispatch.forward(request, response);
////			}
////			
////		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/login")) {
//			response.setContentType("text/html;charset=utf-8");
//			memberDAO = new MemberDAO();
//			printWriterOut = response.getWriter();
//			session = request.getSession();
//			String loginId = request.getParameter("loginId");
//			String loginPwd = request.getParameter("loginPwd");
//			memberBean = memberDAO.viewMember(loginId);
//			
//			System.out.println("login : " + memberBean);
//			
//				if(memberBean != null && memberBean.getMemberPwd().equals(loginPwd)) {
//					session.setAttribute("sess_memberBean", memberBean);
//					response.sendRedirect("/homepageProject/homepageSub.jsp");
//					printWriterOut.print("안녕하세요 " + loginId + "님!!!");
//				} else {
//					RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/login.jsp");
//					dispatch.forward(request, response);
//					printWriterOut.print("<a href='/homepageProject/jsp/member/login.jsp'>다시 로그인 하세요!!</a>");
//				}
//		}
//		
////		if (request.getRequestURI().equals("/homepageProject/member/withdrawalMember")) {
////			response.setContentType("text/html;charset=utf-8");
////			memberBean = (MemberBean) request.getSession().getAttribute("sess_memberBean");
////			memberDAO = new MemberDAO();
////			boolean flag = memberDAO.withdrawalMember(memberBean);
////			if (flag) {
////				
////			} else {
////				
////			}
////			printWriterOut = response.getWriter();
////		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/withdrawalMember")) {
//			response.setContentType("text/html;charset=utf-8");
//			memberBean = (MemberBean) request.getSession().getAttribute("sess_memberBean");
//			memberDAO = new MemberDAO();
//			jsonResult = new JSONObject();
//			
//			boolean flag = memberDAO.withdrawalMember(memberBean);
//			if (flag) {
//				jsonResult.put("message", "탈퇴가 완료되었습니다.");
//				jsonResult.put("url", "/homepageProject/homepageMain.jsp");
//			} else {
//				
//			}
//			printWriterOut = response.getWriter();
//			printWriterOut.println(jsonResult.toString());
//			
//		}
//		
//		
//		if (request.getRequestURI().equals("/homepageProject/member/list")) {
//			memberDAO = new MemberDAO();
//			request.setAttribute("listMembers", memberDAO.listMembers());
//
//			RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/member/list.jsp");
//			dispatch.forward(request, response);
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/dupUidCheck")) {
//			String memberId = request.getParameter("memberId");
//			
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.viewMember(memberId);
//			jsonResult = new JSONObject();
//			printWriterOut = response.getWriter();
//			
//			if (memberBean == null) {
//				jsonResult.put("status", true);
//				jsonResult.put("message", "해당 아이디는 사용 가능합니다.");
//			} else {
//				jsonResult.put("status", false);
//				jsonResult.put("message", "해당 아이디는 이미 사용중 입니다.");
//			}
//			printWriterOut = response.getWriter();
//			printWriterOut.println(jsonResult.toString());
//			
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/findId")) {
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.findId(memberName, memberEmail);
//
//			if (memberBean == null) {
//				request.setAttribute("message", "검색 결과가 없습니다.");
//			} else {
//				request.setAttribute("message", "아이디는 " + memberBean.getMemberId() + "입니다.");
//			}
//			RequestDispatcher dispatchId = request.getRequestDispatcher("/jsp/member/findIdResurt.jsp");
//			dispatchId.forward(request, response);
//			
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/findPwd")) {
//			String memberId = request.getParameter("memberId");
//			String phone = request.getParameter("phone");
//			
//			memberDAO = new MemberDAO();
//			memberBean = memberDAO.findPwd(memberId, phone);
//			
//			if (memberBean == null) {
//				request.setAttribute("message", "검색 결과가 없습니다.");
//			} else {
//				request.setAttribute("message", "비밀번호는 " + memberBean.getMemberPwd() + "입니다.");
//			}
//			RequestDispatcher dispatchPwd = request.getRequestDispatcher("/jsp/member/findPwdResurt.jsp");
//			dispatchPwd.forward(request, response);
//				
//		}
//		
//		if (request.getRequestURI().equals("/homepageProject/member/insert")) {
//			String memberId = request.getParameter("memberId");
//			String memberPwd = request.getParameter("memberPwd");
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			String phone = request.getParameter("phone");
//			String address = request.getParameter("address");
//			String birthday = request.getParameter("birthday");
//			String gender = request.getParameter("gender");
//			
//			
//			jsonResult = new JSONObject();
//			
//			memberDAO = new MemberDAO();
//			printWriterOut = response.getWriter();
//			try {
//				memberBean = MemberBean.builder()
//					.memberId(memberId)
//					.memberPwd(memberPwd)
//					.memberName(memberName)
//					.memberEmail(memberEmail)
//					.phone(phone)
//					.address(address)
//					.birthday(birthday)
//					.gender(gender)
//					.build();
//				memberDAO.insertMember(memberBean);
//System.out.println("insert: " + memberBean);
//			} catch (SQLException e) {
//				e.printStackTrace();
//				jsonResult.put("status", false);
//				jsonResult.put("message", "해당아이디는 사용하실 수 없습니다.");
//				printWriterOut.print(jsonResult);
//				return;
//			}
//			
//			
////			BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
////			String jsonStr = in.readLine();
////			System.out.println("jsonStr = " + jsonStr);
////			JSONObject jsonMember = new JSONObject(jsonStr);
//			
//			jsonResult.put("status", true);
//			jsonResult.put("url", "/homepageProject/homepageMain.jsp");
//			jsonResult.put("message", "가입을 환영합니다.");
//			
//			
//			printWriterOut.print(jsonResult);
//		}
//
//		if (request.getRequestURI().equals("/homepageProject/member/updateMemberInfo")) {
//			
//			System.out.println("heheheheheh");
//			memberBean = (MemberBean) request.getSession().getAttribute("sess_memberBean");
//			String memberId = memberBean.getMemberId();
//			String memberPwd = request.getParameter("memberPwd");
//			String memberName = request.getParameter("memberName");
//			String memberEmail = request.getParameter("memberEmail");
//			String phone = request.getParameter("phone");
//			String address = request.getParameter("address");
//			String birthday = request.getParameter("birthday");
//			String gender = request.getParameter("gender");
//			Date joinDate = memberBean.getJoinDate();
//			
//			System.out.println("update: " + memberPwd + " " + memberName + " " + memberName + " " + memberEmail + " " +  phone+ " " + address + " " + birthday + " " + gender);
//			
//			jsonResult = new JSONObject();
//			
//			memberDAO = new MemberDAO();
//			printWriterOut = response.getWriter();
//			try {
//				MemberBean updateMember = MemberBean.builder()
//					.memberId(memberId)
//					.memberPwd(memberPwd)
//					.memberName(memberName)
//					.memberEmail(memberEmail)
//					.phone(phone)
//					.address(address)
//					.birthday(birthday)
//					.gender(gender)
//					.joinDate(joinDate)
//					.build();
//				System.out.println("update: " + memberDAO.updateMember(updateMember));
//				request.getSession().setAttribute("sess_memberBean", updateMember);
//				
//			} catch (SQLException e) {
//				e.printStackTrace();
//				jsonResult.put("status", false);
//				jsonResult.put("message", "아이디를 찾을 수 없습니다.");
//				printWriterOut.print(jsonResult);
//				return;
//			}
//			
//			jsonResult.put("status", true);
//			jsonResult.put("url", "/homepageProject/jsp/member/view.jsp");
//			jsonResult.put("message", "수정이 완료되었습니다..");
//			
//			
//			printWriterOut.print(jsonResult);
//		}
//		
//	}
}
