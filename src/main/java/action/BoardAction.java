package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.BoardBean;
import board.BoardDAO;

public class BoardAction {

	public String postList(HttpServletRequest request, HttpServletResponse response) throws Exception {
			BoardDAO boardDAO = new BoardDAO();
			request.setAttribute("listBoards", boardDAO.listBoards());

//		if (request.getRequestURI().equals("/homepageProject/storyboard/postList")) {
//			request.setCharacterEncoding("UTF-8");
//			BoardDAO boardDAO = new BoardDAO();
//	
//			List<BoardBean> list = boardDAO.listBoards();
//			request.setAttribute("listBoards", list);
//			boardDAO.close();
//		}
			return "/jsp/storyBoard/postList.jsp";
	}
	
	public String postDetail(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
		
		BoardDAO boardDAO = new BoardDAO();
		BoardBean boardBean = boardDAO.postDetail(boardNo);
		if (boardBean.getBoardNo() == (boardNo)) {
			request.setAttribute("boardBean", boardBean);
		}
		
		return "/jsp/storyBoard/postDetail.jsp";
	}
		
	
//	
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		BoardDAO boardDAO;
//		BoardFileDAO boardFileDAO;
//		BoardBean boardBean;
//		BoardFileBean boardFileBean;
//		JSONObject jsonResult;
//		PrintWriter printWriterOut;
//
//		System.out.println("BoardSevlet============ : " + request.getRequestURI());
//
//// 게시글 상세보기
//		if (request.getRequestURI().equals("/homepageProject/storyboard/postDetail")) {
//
//			int boardNo = Integer.parseInt(request.getParameter("boardNo"));
//
//			boardDAO = new BoardDAO();
//			boardBean = boardDAO.postDetail(boardNo);
//			if (boardBean.getBoardNo() == (boardNo)) {
//				request.setAttribute("boardBean", boardBean);
//				RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/storyboard/postDetail.jsp");
//				dispatch.forward(request, response);
//			}
//
//		}
//
//// 게시글 목록		
//		if (request.getRequestURI().equals("/homepageProject/storyboard/postList")) {
//			boardDAO = new BoardDAO();
//			request.setAttribute("listBoards", boardDAO.listBoards());
//
//			RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/storyboard/postList.jsp");
//			dispatch.forward(request, response);
//		}
//
//// 게시글 등록		
//		if (request.getRequestURI().equals("/homepageProject/storyboard/insert")) {
//			System.out.println("파일이 들어오나요");
//			// 저장소 객체 새성
//			DiskFileItemFactory factory = new DiskFileItemFactory();
//
//			// 업로드 파일 임시로 저장할 경로 설정
//			factory.setRepository(new File("c:\\upload"));
//
//			// 파일 업로드 객체에 저장소 설정
//			ServletFileUpload upload = new ServletFileUpload(factory);
//
//			System.out.println(request.getRemoteAddr());
//			// 요청 객체를 파싱한다
//			try {
//				Map<String, List<FileItem>> mapItems = upload.parseParameterMap(request);
//
//				String postTitle = new String(mapItems.get("postTitle").get(0).getString().getBytes("ISO-8859-1"),
//						"UTF-8");
//				String postContent = new String(mapItems.get("postContent").get(0).getString().getBytes("ISO-8859-1"),
//						"UTF-8");
//				String postWriter = ((MemberBean) request.getSession().getAttribute("sess_memberBean")).getMemberId();
//
//				jsonResult = new JSONObject();
//
//				boardDAO = new BoardDAO();
//				boardFileDAO = new BoardFileDAO();
//
//				printWriterOut = response.getWriter();
//
//				// 첨부파일 정보 얻어 저장
//				FileItem fileItem = mapItems.get("filename1").get(0);
//				String real_name = "c:\\upload\\" + System.nanoTime();
//				fileItem.write(new File(real_name));
//				boardBean = BoardBean.builder().postTitle(postTitle).postContent(postContent).postWriter(postWriter)
//						.build();
//				try {
//					int boardNo = boardDAO.insertPost(boardBean);
//					boardFileBean = new BoardFileBean(0, boardNo, fileItem.getName(), real_name,
//							fileItem.getContentType(), fileItem.getSize());
//					boardFileDAO.insertPostFile(boardFileBean);
//
//					boardFileDAO.close();
//					boardDAO.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//					jsonResult.put("status", false);
//					jsonResult.put("message", "공지사항을 확인해 주세요.");
//					printWriterOut.print(jsonResult);
//					return;
//				}
//
//				jsonResult.put("status", true);
//				jsonResult.put("url", "/homepageProject/storyboard/postList");
//				jsonResult.put("message", "매너있는 글 작성 감사합니다.");
//
//				printWriterOut.print(jsonResult);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//// 게시글 수정			
//			if (request.getRequestURI().equals("/homepageProject/storyboard/updatePostForm")) {
//
//				System.out.println("updatePost start ~");
//				int boardNo = Integer.parseInt(request.getParameter("boardNo"));
//
//				boardDAO = new BoardDAO();
//				boardBean = boardDAO.postDetail(boardNo);
//				if (boardBean.getBoardNo() == (boardNo)) {
//					request.setAttribute("boardBean", boardBean);
//					RequestDispatcher dispatch = request.getRequestDispatcher("/jsp/storyboard/updatePost.jsp");
//					dispatch.forward(request, response);
//				}
//
//			}
//
//			else if (request.getRequestURI().equals("/homepageProject/storyboard/updatePost")) {
//
//				System.out.println("updatePost start ~");
//				String postTitle = request.getParameter("postTitle");
//				String postContent = request.getParameter("postContent");
//				int boardNo = Integer.parseInt(request.getParameter("boardNo"));
//
//				System.out.println("update 1: " + postTitle + ", " + postContent);
//
//				jsonResult = new JSONObject();
//
//				boardDAO = new BoardDAO();
//				printWriterOut = response.getWriter();
//				try {
//					BoardBean updateBoard = BoardBean.builder().boardNo(boardNo).postTitle(postTitle)
//							.postContent(postContent).build();
//					System.out.println("update 2: " + boardDAO.updatePost(updateBoard));
//					jsonResult.put("status", true);
//					jsonResult.put("message", "글 수정 완료!");
//					jsonResult.put("url", "/homepageProject/storyboard/postDetail?boardNo=" + updateBoard.getBoardNo());
//				} catch (SQLException e) {
//					e.printStackTrace();
//					jsonResult.put("status", false);
//					jsonResult.put("message", "해당 글을 찾을 수 없습니다.");
//				}
//				printWriterOut.print(jsonResult);
//			}
//
//// 게시글 삭제			
//			if (request.getRequestURI().equals("/homepageProject/storyboard/deletePost")) {
//
//				System.out.println("deletePost start ~");
//				int boardNo = Integer.parseInt(request.getParameter("boardNo"));
//
//				boardDAO = new BoardDAO();
//				boardDAO.deletePost(boardNo);
//
//				// 방법 1
//				// RequestDispatcher dispatch =
//				// request.getRequestDispatcher("/storyboard/postList");
//				// dispatch.forward(request, response);
//
//				// 방법 2
//				response.sendRedirect("/homepageProject/storyboard/postList");
//
//			}
//
//		}
//
//	}
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		doGet(request, response);
//	}
}
